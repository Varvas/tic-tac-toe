import React from 'react';
import Main from './screens/main'
import Statistics from './screens/stats'
import Settings from './screens/settings'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import { createStore } from 'redux';
import { Provider } from 'react-redux'
import reducer from './reducers/index';


const Tab = createBottomTabNavigator();
let store = createStore(reducer);


const App = () => {
  return (
    <>
    <Provider store={store}>
    <NavigationContainer>
      <Tab.Navigator 
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: 'blue',
      }}>
        <Tab.Screen name="Home"
          options={{
          tabBarLabel: 'Game',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="gamepad-variant" color={color} size={30} />
          ),
        }} component={Main}/>
        <Tab.Screen name="Statistics" 
          options={{
          tabBarLabel: 'Statistics',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="chart-bar" color={color} size={30} />
          ),
        }} component={Statistics}/>
        <Tab.Screen name="Settings" 
          options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({color}) => (
            <Octicons name="settings" color={color} size={30} />
          ),
        }} component={Settings}/>
      </Tab.Navigator>
    </NavigationContainer>
    </Provider>
    </>
  );
};


export default App;
