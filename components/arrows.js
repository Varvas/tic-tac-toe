import React from 'react'
import {StyleSheet, Text, View} from 'react-native'
import Entypo from 'react-native-vector-icons/Entypo';

export const RedArrow = ()=> {

    return(
        <View style={styles.container}>
        <Entypo name="arrow-bold-left" size={80} style={[styles.arrow, {color: 'red'}]}/>
        <Text style={{fontSize: 15, color: 'red'}}>Next</Text>
        </View>
    )
   
}

export const BlueArrow = ()=> {

    return(
        <View style={styles.container}>
        <Text style={{fontSize: 15, color: 'blue'}}>Next</Text>
        <Entypo name="arrow-bold-right" size={80} style={[styles.arrow, {color: 'blue'}]}/>
        </View>
    )
   
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', 
        width: 110, 
        alignItems: 'center',
        justifyContent: 'center'
    },
    arrow: {
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
      }
})