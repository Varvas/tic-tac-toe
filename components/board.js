import React from 'react'
import { View, StyleSheet } from 'react-native'
import Box from './box';

const Board = ({squares,  onPress})=> {

  return(
  <View style={styles.board}>
    {squares.map((square, i)=> (
      <Box key={i} value={square} onPress={()=> onPress(i)}/>
    ))}
    
  </View>
  )
  
}

const styles = StyleSheet.create({
  board: {
   justifyContent: 'center',
   flexDirection: 'row',
   flexWrap: 'wrap'
  }
})

export default Board;