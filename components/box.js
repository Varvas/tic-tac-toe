import React from 'react'
import { TouchableOpacity, StyleSheet, Text } from 'react-native'

const Box = ({onPress, value})=> {
    return(
        <TouchableOpacity onPress={onPress} style={styles.box}>
            <Text style={[styles.value,{color: value === "X"? 'red' : 'blue'}]}>{value}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    box: {
        width: 100,
        height: 100,
        borderColor: '#000',
        borderWidth: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    value: {
        fontSize: 40, 
        fontWeight: 'bold',
    }
})

export default Box;