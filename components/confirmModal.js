import React from 'react'
import {View, Text, Modal, StyleSheet, TouchableOpacity } from 'react-native'


const ConfirmModal = ({visible, onPressCancel, onPressOK }) => {

    
    return (
          <Modal
            animationType="slide"
            transparent={true}
            visible={visible}>
          <View style={styles.centeredView}>
           <View style={styles.modalView}>
            <Text style={styles.modalText}>Are you sure you want to Reset Statistics?</Text>
            <View style={{paddingHorizontal: 10, width: '80%', height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>

                <TouchableOpacity
                style={styles.openButton}
                onPress={onPressCancel}>
                <Text style={[styles.textStyle, {color: 'red'}]}>Cancel</Text>
                </TouchableOpacity>

                <TouchableOpacity
                style={styles.openButton}
                onPress={onPressOK}>
                <Text style={[styles.textStyle, {color: '#08a112'}]}>OK</Text>
                </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  };

  const styles = StyleSheet.create({
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        width: '80%',
        height: 160,
        backgroundColor: "#ededed",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        justifyContent: 'space-between'
      },
      openButton: {
        alignItems: 'center',
        justifyContent: 'center',
      },
      textStyle: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 20,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 5
      },
      modalText: {
        fontSize: 17,
        marginBottom: 15,
        textAlign: "center",
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 5
      }
  })
  
  
  export default ConfirmModal;
  