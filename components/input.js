import React from 'react'
import { View, Text, TextInput, StyleSheet} from 'react-native'
import Submit from './submit'


const Input = ({onPress, value, onChangeText})=> {
    return(
        <View style={styles.container}>
        <TextInput style={styles.text}
         value={value} placeholder="Name" onChangeText={onChangeText}/>
        <Submit onPress={onPress}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 100, 
        width: '80%'
    },
    text: {
        width: '100%', 
        marginBottom: 30, 
        borderBottomColor: 'grey', 
        borderBottomWidth: 1
    }
})

export default Input;