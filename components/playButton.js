import React from 'react'
import { TouchableOpacity, Text, StyleSheet} from 'react-native'


const PlayButton = ({onPress, title})=> {
    return(
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    touchable: {
        width: 120, 
        height: 50, 
        backgroundColor: '#08a112', 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderWidth: 2, 
        borderColor: '#fff', 
        borderRadius: 15, 
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    title: {
        color: "#fff", 
        fontSize: 22, 
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    }
})

export default PlayButton;