import React from 'react'
import { TouchableOpacity, Text, StyleSheet} from 'react-native'


const ResetButton = ({onPress})=> {
    return(
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
            <Text style={styles.title}>Reset Statistics</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    touchable: {
        flex: 1, 
        width: '100%', 
        alignItems: 'center', 
        justifyContent: 'center',
        borderBottomWidth: 1, 
        borderStyle: 'solid', 
        borderBottomLeftRadius: 5, 
        borderBottomRightRadius: 5,
        borderBottomColor: 'red',
        backgroundColor: 'red'
    },
    title: {
        color: '#fff', 
        fontSize: 25, 
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 5
    }
})

export default ResetButton;