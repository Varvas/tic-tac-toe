import React from 'react'
import { TouchableOpacity, Text, StyleSheet} from 'react-native'


const Submit = ({onPress})=> {
    return(
        <TouchableOpacity style={styles.touchable} onPress={onPress}>
            <Text style={styles.title}>Submit</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    touchable: {
        width: 100, 
        height: 40, 
        backgroundColor: '#08a112', 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderWidth: 2, 
        borderColor: '#fff', 
        borderRadius: 15, 
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    title: {
        color: "#fff", 
        fontSize: 18, 
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    }
})

export default Submit;