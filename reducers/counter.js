
export const counter = (state = 0, {type})=> {

    switch(type){
        case "INCRESE" :
         return state + 1;
        case "RESET" :
            return state = 0;
         default:
             return state;
    }

}

export const increment = ()=> ({
    type: "INCRESE"
})

export const reset = ()=> ({
    type: "RESET"
})