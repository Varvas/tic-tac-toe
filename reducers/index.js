import { combineReducers } from 'redux'
import {addPlayers} from './players'
import {counter} from './counter'
import {score} from './score'
import {startTheGame} from './start'

export default combineReducers({
  players: addPlayers,
  count: counter,
  scores: score,
  start: startTheGame
})