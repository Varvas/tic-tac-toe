
const initialState = {
    player1: null,
    player2: null
}

export const addPlayers = (state=initialState, {type, payload})=> {
    switch(type){
        case "ADD_PLAYER_1":
            return {
                ...state,
                player1: payload
            };
        case "ADD_PLAYER_2":
            return {
                ...state,
                player2: payload
            };
        default:
            return state;
    }
}

export const addPlayer1Action = name=> ({
        type: "ADD_PLAYER_1",
        payload: name
})

export const addPlayer2Action = name=> ({
        type: "ADD_PLAYER_2",
        payload: name
})