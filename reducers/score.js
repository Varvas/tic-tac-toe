
const initialState = {
    p1: 0,
    p2: 0
}

export const score = (state = initialState, {type})=> {

    switch(type){
        case "P1_WIN":
            return {
                ...state,
                p1: state.p1 + 1
            }
        case "P2_WIN":
            return {
                ...state,
                p2: state.p2 + 1
            }
        case "RESET_SCORE":
            return {
                ...state,
                p1: 0,
                p2: 0,
            }
        default: 
        return state;
    }
}

export const P1WinsAction = ()=> ({
   type: "P1_WIN"
});

export const P2WinsAction = ()=> ({
    type: "P2_WIN"
});

export const ResetScoreAction = ()=> ({
    type: "RESET_SCORE"
});