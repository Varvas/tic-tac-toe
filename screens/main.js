import React, {useState, useEffect} from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Board from '../components/board';
import {calculateWinner as Logic} from '../logic/logic';
import {RedArrow, BlueArrow} from '../components/arrows'
import PlayButton from '../components/playButton'
import {increment} from '../reducers/counter'
import {P1WinsAction, P2WinsAction} from '../reducers/score'
import {useSelector, useDispatch} from 'react-redux'


const Main = () => {
  
  const [board, setBoard] = useState(Array(9).fill(null));
  const [xIsNext, setXisNext] = useState(true);
  const winner = Logic(board);
  const dispatch = useDispatch();
  const user1 = useSelector(state=> state.players.player1);
  const user2 = useSelector(state=> state.players.player2);
  const user1Score = useSelector(state=> state.scores.p1);
  const user2Score = useSelector(state=> state.scores.p2);
  const started = useSelector(state=> state.start)

  useEffect(()=> {
    winner === 'X' && dispatch(P1WinsAction());
    winner === 'O' && dispatch(P2WinsAction());
  },[winner])
  

  const handleClick = i => {
    if(winner || board[i]) return;
    board[i] = xIsNext? 'X' : 'O' ;
    setBoard(board);
    setXisNext(!xIsNext);  
  }

  const checkScore = ()=>{
    let status;
    let won = winner === 'X'? user1 : user2;
    if(winner && winner != 'draw'){
      status = 'Winner: ' + won;
  } else if (winner && winner === 'draw'){
      status = "It's a " + winner;
  } 
    return status;
  }


  const restart = ()=> {
    dispatch(increment());
    setBoard(Array(9).fill(null));
  }

  return (
    <>
     <View style={styles.container}>
       <View style={styles.header}>
        <Text style={styles.title}>TIC TAC TOE</Text>
       </View>
       {started ?<><View style={styles.body}>
        <Text style={styles.scoreTitle}>SCORES</Text>
        <View style={styles.userName}>
        <Text style={[styles.title, {color: 'red'}]}>{user1}</Text>
        <Text style={[styles.title, {color: 'blue'}]}>{user2}</Text>
        </View>
        <View style={styles.score}>
        <Text style={[styles.title, {color: 'red'}]}>{user1Score}</Text>
        <Text style={[styles.title, {color: 'blue'}]}>{user2Score}</Text>
        </View>
        <Board squares={board} onPress={handleClick}/>
        <View style={{height: 10, alignItems: 'center', justifyContent: 'center'}}>
         <Text style={[styles.winner, {color: winner === 'X'? 'red' : 'blue'}]}>{checkScore()}</Text> 
        </View>
       </View>
       <View style={styles.footer}>
         <View style={styles.items}>
         {xIsNext && <RedArrow/>}
         </View>
         <View style={styles.items}>
           {winner && <PlayButton
           title={'RESTART'}
           onPress={()=> restart()}
           />}
         </View>
         <View style={styles.items}>
         {!xIsNext && <BlueArrow/>}
         </View>
       </View>
       </>
      :
      <View style={styles.bodyText}>
        <Text style={styles.welcomeText}>Wellcome!</Text>
        <Text style={styles.welcomeText}>Please setup player names</Text>
        <Text style={styles.welcomeText}>and start the Game.</Text>
      </View>
      }

     </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgrey'
  },
  header: {
    height: 50,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    backgroundColor: '#fff'
  },
  title: {
      fontSize: 28,
      fontWeight: 'bold',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 1, height: 1},
      textShadowRadius: 5
  },
  body: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 50,
      margin: 10,
      backgroundColor: '#fff',
      borderRadius: 15,
      shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
      shadowOpacity: 0.39,
      shadowRadius: 8.30,
      elevation: 13,
  },
  bodyText: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    backgroundColor: '#fff',
    borderRadius: 15,
    shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 6,
      },
    shadowOpacity: 0.39,
    shadowRadius: 8.30,
    elevation: 13,
},
  scoreTitle: {
    fontSize: 30,
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 5,
    color: '#249c00'
  },
  userName: {
    height: 10, 
    width: '90%', 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    flexDirection: 'row'
  },
  score: {
    height: 10, 
    paddingHorizontal: 30, 
    width: '90%', 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    flexDirection: 'row'
  },
  items: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  winner: {
    fontSize: 25,
    fontWeight: 'bold'
  },
  itemContainer: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  footer: {
    flexDirection: 'row',
    width: '100%',
    height: 100,
    backgroundColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10
  },
  welcomeText:{
    fontSize: 25,
    color: 'grey',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 5,
  }
});

export default Main;
