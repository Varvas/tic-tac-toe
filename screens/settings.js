import React, {useState} from 'react'
import {View, Text, StyleSheet } from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import { addPlayer1Action, addPlayer2Action } from '../reducers/players'
import { reset } from '../reducers/counter'
import Input from '../components/input'
import {ResetScoreAction} from '../reducers/score'
import PlayButton from '../components/playButton'
import ResetButton from '../components/resetButton'
import ConfirmModal from '../components/confirmModal'
import {increment} from '../reducers/counter'
import {startAction} from '../reducers/start'

const Settings = ({navigation}) => {

    const user1 = useSelector(state=> state.players.player1);
    const user2 = useSelector(state=> state.players.player2);
    const stateCounter = useSelector(state=> state.count);

    const dispatch = useDispatch();

    const [player1, setPlayer1] = useState('');
    const [player2, setPlayer2] = useState('');
    const [modalVisible, setModalVisible] = useState(false);

    const handlePress1 = ()=> {
        if(player1 !== ''){
            dispatch(addPlayer1Action(player1));
            setPlayer1('');
        } 
    }

    const handlePress2 = ()=> {
        if(player2 !== ''){
            dispatch(addPlayer2Action(player2));
            setPlayer2('');
        } 
    }

    const playPressed = ()=> {
      if(stateCounter === 0){
        dispatch(increment());
        dispatch(startAction());
        navigation.navigate('Home');
      }
        navigation.navigate('Home');
    }

    const closeModal = ()=> {
      dispatch(reset());
      dispatch(ResetScoreAction());
      setModalVisible(!modalVisible)
    }

    return (
        
        <View style={styles.container}>
          <ConfirmModal
          visible={modalVisible}
          onPressCancel={()=> setModalVisible(!modalVisible)}
          onPressOK={()=>closeModal()}
          />
          <View style={styles.body}>
            <Text style={styles.title}>Player's Settings</Text>
              <View style={styles.inputContainer}>

                <View style={{width: '90%'}}>
                    <Text style={{fontSize: 25}}>Player 1<Text style={{color: 'red'}}>: {user1}</Text></Text>
                    <Input
                    title={'Player 1'}
                    value={player1}
                    storeValue={user1}
                    onChangeText={e=> setPlayer1(e)}
                    onPress={()=>handlePress1()}
                    />
                </View>

                <View style={{width: '90%'}}>
                    <Text style={{fontSize: 25}}>Player 2<Text style={{color: 'blue'}}>: {user2}</Text></Text>
                    <Input
                    title={'Player 2'}
                    value={player2}
                    storeValue={user2}
                    onChangeText={e=> setPlayer2(e)}
                    onPress={()=>handlePress2()}
                    />
                </View>

                <View style={{height: 10}}>
                {user1 && user2 !== null ? <PlayButton title={'PLAY'} onPress={()=> playPressed()}/> : null}
                </View>
                
              </View>

              <View style={styles.footer}>
                {stateCounter >=1 && <ResetButton onPress={()=> setModalVisible(!modalVisible)}/>}
              </View>
            
          </View>
        </View>
        
    );
  };

  const styles = StyleSheet.create({
      container: {
        flex: 1, 
        padding: 10,
      },
      title: {
        fontSize: 40,
        color: '#160f30',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
      },
      body: {
        flex: 1,
        backgroundColor: '#fff', 
        borderRadius: 5, 
        borderWidth: 1, 
        borderColor: '#ededed', 
        flexDirection: 'column', 
        alignItems: 'center', 
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.39,
        shadowRadius: 8.30,
        elevation: 13,
      },
      inputContainer: {
        flex: 5,
        marginTop: 30, 
        paddingBottom: 20, 
        width: '100%', 
        flexDirection: 'column', 
        alignItems: 'center', 
        justifyContent: 'space-around',
      },
      footer: {
        flex: 0.5, 
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
      },
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
  })
  
  
  export default Settings;
  