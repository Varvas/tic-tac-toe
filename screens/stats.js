import React from 'react'
import {View, Text, StyleSheet } from 'react-native'
import {useSelector} from 'react-redux'

const Statistics = () => {

  const counter = useSelector(state=> state.count);
  const user1 = useSelector(state=> state.players.player1);
  const user2 = useSelector(state=> state.players.player2);
  const user1Score = useSelector(state=> state.scores.p1);
  const user2Score = useSelector(state=> state.scores.p2);

    return (
        <View style={styles.container}>
          <View style={styles.body}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>STATISTICS</Text>
            </View>
            <View style={styles.counterContainer}>
              <Text style={styles.counterTitle}>Total Rounds:</Text>
              <Text style={styles.counterTitle}>{counter}</Text>
            </View>
            <View style={styles.counterContainer}>
              <Text style={styles.counterTitle}>Total Wins:</Text>
              {counter !==0 ? <><Text style={{fontSize: 35, color: 'red'}}>{`${user1} : ${user1Score}`}</Text>
              <Text style={{fontSize: 35, color: 'blue'}}>{`${user2} : ${user2Score}`}</Text></>
            :
            <Text style={{fontSize: 35, color: 'grey'}}>No Games Played</Text>  
            }
            </View>
          </View>
        </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      padding: 10,
    },
    titleContainer: {
      height: 50,
      alignItems: 'center',
      justifyContent: 'center'
    },
    title: {
      fontSize: 40,
      color: 'lightgrey',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 1, height: 1},
      textShadowRadius: 5,
    },
    body: {
      flex: 1,
      backgroundColor: '#fff', 
      borderRadius: 5, 
      borderWidth: 1, 
      paddingTop: 20,
      borderColor: '#ededed', 
      flexDirection: 'column', 
      alignItems: 'center', 
      justifyContent: 'flex-start',
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 6,
      },
      shadowOpacity: 0.39,
      shadowRadius: 8.30,
      elevation: 13,
    },
    counterContainer: {
      width: '80%',
      height: 200,
      flexDirection: 'column',
      borderTopColor: 'grey',
      borderTopWidth: 1,
      marginTop: 20,
      alignItems: 'center',
      justifyContent: 'space-evenly'
    },
    counterTitle: {
      fontSize: 40,
      color: '#3d423c',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 1, height: 1},
      textShadowRadius: 5,
    },
  })
  
  export default Statistics;
  